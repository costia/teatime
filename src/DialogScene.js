import Phaser from "phaser";
var musicPlaying = false;

export class TeaTimeDialog extends Phaser.Scene {
  constructor(levelName, callback, preloadCallback) {
    super({
      key: levelName,
    });
    this.levelName = levelName;
    this.callback = callback;
    this.preloadCallback = preloadCallback;
  }
  preload() {
    this.load.json(
      this.levelName + "JSON",
      "assets/" + this.levelName + ".json"
    );
    this.load.once("filecomplete", () => {
      let jsonIn = this.cache.json.get(this.levelName + "JSON")[0];
      this.load.image(jsonIn.background, jsonIn.background);

      if (jsonIn.music) {
        this.load.audio(jsonIn.music, jsonIn.music, {
          instances: 1,
        });
      }
    });
  }
  create() {
    this.preloadCallback();
    let jsonData = this.cache.json.get(this.levelName + "JSON")[0];
    this.add.sprite(300, 300, jsonData.background).setOrigin(0.5);
    this.add
      .text(300, 500, jsonData.text, {
        fontSize: String(jsonData.fontSize) + "px",
        fontFamily: jsonData.font,
        fill: jsonData.color,
        align: "center",
      })
      .setOrigin(0.5);
    if (jsonData.music) {
      if (musicPlaying) {
        musicPlaying.stop();
      }
      musicPlaying = this.sound.add(jsonData.music, {
        loop: true,
        volume: 0.1,
      });
      musicPlaying.play();
    }

    setTimeout(() => {
      this.callback();
    }, 3000);
  }
  update() {}
}
