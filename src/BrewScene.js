export class TeaTimeBrew extends Phaser.Scene {
  constructor(levelName, callback, preloadCallback) {
    super({
      key: levelName,
    });
    this.levelName = levelName;
    this.callback = callback;
    this.preloadCallback = preloadCallback;
    this.nextID = 1;
    this.gameObjects = {};
    this.objectTypes = {};
  }
  preload() {
    this.load.json(
      "objectTypes_" + this.levelName,
      "assets/ObjectTypes_" + this.levelName + ".json"
    );
    this.load.once(
      "filecomplete",
      () => {
        let jsonIn = this.cache.json.get("objectTypes_" + this.levelName);
        for (let ind = 0; ind < jsonIn.length; ind++) {
          this.load.image(
            jsonIn[ind].typeID + "_sprite_" + this.levelName,
            jsonIn[ind].spritePath
          );
        }
      },
      this
    );
  }

  createObject(typeID, x, y) {
    let obj = {};
    obj.renderedObject = this.add
      .sprite(x, y, typeID + "_sprite_" + this.levelName)
      .setOrigin(0.5);
    obj.renderedObject.setInteractive();
    obj.typeID = typeID;
    obj.renderedObject.ID = this.nextID;
    obj.ID = this.nextID;
    this.gameObjects[obj.ID] = obj;
    this.nextID++;
    return obj;
  }

  create() {
    this.preloadCallback();
    let jsonIn = this.cache.json.get("objectTypes_" + this.levelName);
    for (let ind = 0; ind < jsonIn.length; ind++) {
      this.objectTypes[jsonIn[ind].typeID] = jsonIn[ind];
    }

    this.input.topOnly = false;
    this.cameras.main.setBackgroundColor("rgba(255, 255, 255, 1)");
    this.add
      .sprite(300, 300, "background_sprite_" + this.levelName)
      .setOrigin(0.5);

    let url = "assets/" + this.levelName + ".json";
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "json";
    xhr.onload = () => {
      let status = xhr.status;
      if (status === 200) {
        let jsonData = xhr.response;
        for (let ind in jsonData) {
          this.createObject(
            jsonData[ind].typeID,
            jsonData[ind].position.x,
            jsonData[ind].position.y
          );
        }
      }
    };
    xhr.send();

    this.currentObject = null;
    this.dragPosition = { x: 0, y: 0, z: 0 };

    this.input.on("pointerdown", (pointer, inputEventObjects) => {
      for (let ind in inputEventObjects) {
        let objData = this.gameObjects[inputEventObjects[ind].ID];
        let objTypeData = this.objectTypes[objData.typeID];

        if (objTypeData.contains) {
          objData = this.createObject(
            objTypeData.contains,
            objData.renderedObject.x,
            objData.renderedObject.y
          );
          objTypeData = this.objectTypes[objData.typeID];
        }
        if (!objTypeData.canDrag) continue;

        this.dragPosition.initX = objData.renderedObject.x;
        this.dragPosition.initY = objData.renderedObject.y;
        this.dragPosition.initZ = objData.renderedObject.depth;

        this.currentObject = objData;
        this.dragPosition.x =
          pointer.position.x - this.currentObject.renderedObject.x;
        this.dragPosition.y =
          pointer.position.y - this.currentObject.renderedObject.y;
        this.currentObject.renderedObject.depth = 100;
        break;
      }
    });
    this.input.on("pointermove", (pointer, inputEventObjects) => {
      if (this.currentObject) {
        this.currentObject.renderedObject.x =
          pointer.position.x - this.dragPosition.x;
        this.currentObject.renderedObject.y =
          pointer.position.y - this.dragPosition.y;
      }
    });

    this.input.on("pointerup", (pointer, inputEventObjects) => {
      if (this.currentObject) {
        this.currentObject.renderedObject.depth = this.dragPosition.initZ;
        this.currentObject.renderedObject.x = this.dragPosition.initX;
        this.currentObject.renderedObject.y = this.dragPosition.initY;
        let thisObjTypeData = this.objectTypes[this.currentObject.typeID];

        for (
          let inputInd = 0;
          inputInd < inputEventObjects.length;
          inputInd++
        ) {
          if (
            inputEventObjects[inputInd] != this.currentObject.renderedObject
          ) {
            let otherObj = this.gameObjects[inputEventObjects[inputInd].ID];
            let otherObjTypeData = this.objectTypes[otherObj.typeID];

            let combineResultType = null;

            let ind = otherObjTypeData.combinesWith.findIndex(element => {
              return element == this.currentObject.typeID;
            });
            if (ind >= 0) {
              combineResultType = otherObjTypeData.combineResult[ind];
            }

            if (ind < 0) {
              ind = thisObjTypeData.combinesWith.findIndex(element => {
                return element == otherObj.typeID;
              });
              if (ind >= 0) {
                combineResultType = thisObjTypeData.combineResult[ind];
              }
            }
            if (ind < 0) continue;

            let pos = {
              x: otherObj.renderedObject.x,
              y: otherObj.renderedObject.y,
            };

            // delete this.objectTypes[this.currentObject.ID];
            delete this.objectTypes[otherObj.ID];

            // this.currentObject.renderedObject.destroy();
            otherObj.renderedObject.destroy();

            this.createObject(combineResultType, pos.x, pos.y);
            if (combineResultType == "teaCup") {
              setTimeout(() => {
                this.callback();
              }, 1000);
            }
            break;
          }
          //console.log(gameObject);
        }
        if (thisObjTypeData.ephemeral) {
          delete this.objectTypes[this.currentObject.ID];
          this.currentObject.renderedObject.destroy();
        }
        this.currentObject = null;
      }
    });
  }
}
