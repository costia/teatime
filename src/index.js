import Phaser from "phaser";
import { TeaTimeDialog } from "./DialogScene";
import { TeaTimeBrew } from "./BrewScene";

export class TeaTimeMain extends Phaser.Scene {
  constructor() {
    super({
      key: "TeaTimeMain",
    });
    this.sessionID = Math.floor(Math.random() * 100000);
    this.levels = [
      {
        type: "dialog",
        name: "level1",
      },
      {
        type: "brew",
        name: "level1",
      },
      {
        type: "dialog",
        name: "level2",
      },
      {
        type: "dialog",
        name: "level3",
      },
      {
        type: "brew",
        name: "level2",
      },
      {
        type: "dialog",
        name: "level4",
      },
    ];
  }
  preload() {}

  preloadCallback() {
    if (this.prevSceneKey) {
      this.scene.remove(this.prevSceneKey);
    }
  }

  loadNextScene() {
    this.prevSceneKey = this.currentSceneKey;

    this.currentSceneKey =
      this.levels[this.nextLevelInd].type +
      "_" +
      this.levels[this.nextLevelInd].name;

    let xhr = new XMLHttpRequest();
    xhr.open("POST", "https://teatime-832b7.firebaseio.com/.json", true);
    let data = {
      sessionID: this.sessionID,
      levelID: this.currentSceneKey,
      currentUrl: window.location.href,
      timeStamp: new Date().toUTCString(),
    };
    xhr.send(JSON.stringify(data));

    if (this.levels[this.nextLevelInd].type == "dialog") {
      this.currentScene = new TeaTimeDialog(
        this.currentSceneKey,
        () => {
          this.loadNextScene();
        },
        () => {
          this.preloadCallback();
        }
      );
    } else if (this.levels[this.nextLevelInd].type == "brew") {
      this.currentScene = new TeaTimeBrew(
        this.currentSceneKey,
        () => {
          this.loadNextScene();
        },
        () => {
          this.preloadCallback();
        }
      );
    }
    this.scene.add(this.currentSceneKey, this.currentScene, true, null);
    this.nextLevelInd++;
    if (this.nextLevelInd >= this.levels.length) {
      this.nextLevelInd = 0;
    }
  }
  create() {
    this.currentScene = null;
    this.currentSceneKey = null;
    this.prevSceneKey = null;
    this.nextLevelInd = 0;

    this.add.text(300, 300, "Loading...", { fontSize: "32px" }).setOrigin(0.5);

    this.loadNextScene();
  }
  update() {}
}

const config = {
  width: 600,
  height: 600,
  parent: "content",
  scene: [TeaTimeMain],
};
const game = new Phaser.Game(config);
